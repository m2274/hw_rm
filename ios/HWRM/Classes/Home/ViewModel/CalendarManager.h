//
//  CalendarManager.h
//  HWRM
//
//  Created by delin.meng on 2020/10/23.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>


NS_ASSUME_NONNULL_BEGIN

@interface CalendarManager : NSObject<RCTBridgeModule>

@property(nonatomic, copy) NSString *name;


@property(nonatomic, copy) NSString *password;


@property (nonatomic, copy) NSString *passwordConfirmation;


@end

NS_ASSUME_NONNULL_END
