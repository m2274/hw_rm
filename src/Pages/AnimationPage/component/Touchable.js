
import React, { PureComponent } from "react";
import {
    TouchableHighlight,
    TouchableNativeFeedback,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Platform
} from 'react-native';
import PropTypes from 'prop-types';

const isAndroid = Platform.OS === 'android';

const propTypes = {
    isHighlight: PropTypes.bool,       // 用于使用TouchableHighlight组件
    isNativeFeedback: PropTypes.bool,  // 仅用于安卓平台，设置原生触摸响应效果
    isWithoutFeedback: PropTypes.bool, // 用于使用TouchableWithoutFeedback组件, 不推荐使用
    children: PropTypes.node,
    activeOpacity: PropTypes.number,   // 用于TouchableOpacity和TouchableHighlight组件，设置触摸响应时不透明度
    underlayColor: PropTypes.string,   // 仅用于TouchableHighlight组件，设置触摸响应时底层颜色
    onPress: PropTypes.func.isRequired // 当触摸操作结束时调用, 处理触摸响应事件
};

const defaultProps = {
    isHighlight: false,
    isNativeFeedback: false,
    isWithoutFeedback: false,
    children: null,
    activeOpacity: 0.8,
    underlayColor: '#e6e6e6'
};

const preventDoublePress = {
    lastPressTime: 1,
    onPress(callback) {
        const curTime = new Date().getTime();
        if (curTime - this.lastPressTime > 1000) {
            this.lastPressTime = curTime;
            callback();
        }
    },
};

class Touchable extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {};
    }

    onPreventDoublePress = () => {
        const { onPress } = this.props;
        if (onPress) {
            preventDoublePress.onPress(onPress)
        }
    }

    render() {
        const {
            children,
            isHighlight,
            isNativeFeedback,
            isWithoutFeedback,
            activeOpacity,
            underlayColor
        } = this.props;

        /** TouchableHighlight 触摸底色高亮响应效果 */
        if (isHighlight) {
            return (
                <TouchableHighlight
                    {...this.props}
                    underlayColor={underlayColor}
                    onPress={this.onPreventDoublePress}
                >
                    {children}
                </TouchableHighlight>
            );
        }

        /** TouchableNativeFeedback Android原生触摸响应效果 */
        if (isAndroid && isNativeFeedback) {
            return (
                <TouchableNativeFeedback
                    {...this.props}
                    delayPressIn={0}
                    background={TouchableNativeFeedback.SelectableBackground()}
                    onPress={this.onPreventDoublePress}
                >
                    {children}
                </TouchableNativeFeedback>
            );
        }

        /** TouchableWithoutFeedback 无视觉反馈触摸响应效果, 建议少使用 */
        if (isWithoutFeedback) {
            return (
                <TouchableWithoutFeedback
                    {...this.props}
                    onPress={this.onPreventDoublePress}
                >
                    {children}
                </TouchableWithoutFeedback>
            );
        }

        /** TouchableOpacity Android/IOS双平台默认触摸响应效果 */
        return (
            <TouchableOpacity
                {...this.props}
                activeOpacity={activeOpacity}
                onPress={this.onPreventDoublePress}
            >
                {children}
            </TouchableOpacity>
        );
    }
}

Touchable.propTypes = propTypes;
Touchable.defaultProps = defaultProps;

export default Touchable